import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';

const NavBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">Burak GUL</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto justify-content-center" navbar>
            <NavItem>
              <NavLink href="https://github.com/burakkggul" target="_blank">GitHub</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://gitlab.com/burakkggul" target="_blank">GitLab</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://www.linkedin.com/in/gulburak/" target="_blank">LinkedIn</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://gitlab.com/burakkggul/under-develop-website" target="_blank">Under Develop Website Project</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
  );
}

export default NavBar;
