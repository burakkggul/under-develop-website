import React from "react";
import { Progress } from "reactstrap";
import "./index.css";
import work from "./development.gif";

const Progresser = (props) => {
  return (
    <div>
      <p className="text-center pb-1">This website is under development.</p>
      <p className="text-center">This Version is <a className="streched-link" href="https://gitlab.com/burakkggul/under-develop-website" target="_blank" rel="noopener noreferrer">here</a></p>
      <img src={work} className="center-work-image" alt="work" />
      <div className="text-center">{"% "+props.info.progress}</div>
      <Progress value={props.info.progress} />
    </div>
  );
};

export default Progresser;
