import React from "react";
import { Row, Container, Col } from "reactstrap";
import NavBar from "./NavBar";
import Progresser from "./Progresser";
import "./index.css";

function App() {
  let progressInfo = {progress:0};
  return (
    <Container>
      <Row>
        <Col>
          <NavBar></NavBar>
        </Col>
      </Row>
      <Row className="pt-5">
        <Col>
          <Progresser info={progressInfo} ></Progresser>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
